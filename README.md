# cartservice

Micro-service gérant le panier, écrit en C# et dotnet.

## Pré-requis pour compiler

Il est nécessaire d'installer le SDK dotnet pour pouvoir compiler l'application.

Il y a plusieurs façons de faire, le plus simple sous Ubuntu est d'utiliser
les dépôts de paquets de Microsoft.  Suivre le tutoriel suivant :

https://learn.microsoft.com/en-us/dotnet/core/install/linux-ubuntu#register-the-microsoft-package-repository

Il faut installer le SDK en version 7.0

**Vérifier** que dotnet est bien installé: `dotnet --version`

## Compilation

Par défaut, dotnet compile le projet sous forme d'un binaire faisant référence
à de nombreux autres fichiers (bibliothèque DLL, runtime dotnet).  Ce n'est pas
très pratique, il faut dans ce cas notamment appeler le binaire depuis le bon répertoire.

Il est aussi possible de compiler le projet en un seul binaire indépendant, ce qui est
plus pratique.  La contrepartie est que le binaire généré est très gros (plusieurs
dizaines de Mo).  Pour générer un tel binaire sous Linux :

    dotnet publish cartservice.csproj -c release --self-contained true -p:PublishSingleFile=true -r linux-x64

Le binaire généré s'appelle `cartservice` et est placé dans le répertoire `bin/release/net7.0/linux-x64/publish/`.

Il est aussi possible de choisir l'emplacement final du binaire via l'option `-o /path/to/cartservice`

_Note_: lorsque vous souhaitez **redéployer**, dotnet ne permet pas de directement remplacer le binaire actuellement exécuté 
(c'est à dire `publish` à l'emplacement du binaire exécuté `/path/to/cartservice`). 
Si vous souhaitez redéployer, il faut faire le remplacement en deux temps: d'abord **publish dans un fichier temporaire**, 
puis **remplacer le binaire exécuté** par ce fichier temporaire.

**Vérifier** que le binaire est bien publié: `ls /path/to/cartservice`

## Lancement du microservice

Le lancement du microservice se fait simplement en exécutant le binaire **depuis le dossier `cartservice/src`**:

    /path/to/cartservice

Par défaut, le service écoute sur localhost uniquement, sur le port 5000.

Pour lancer le service en écoute globale et/ou sur un autre port, il faut définir
la variable `ASPNETCORE_URLS` :

    ASPNETCORE_URLS="http://*:1234" /path/to/cartservice
